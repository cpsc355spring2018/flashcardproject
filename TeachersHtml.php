<?php
$pageTitle = "Teacher Page";
include 'header.php';
include 'webFunctions.php';

if(!isset($_SESSION['key']) && isset($_SESSION['admin']))
{
    $_SESSION['error'] = 'You must be a teacher to view teacher courses.';
    header("Location: loginHtml.php");
    exit();
}
?>
 <style>
 #course{
	background: yellow;
	width: 25%;
	height: 40px;
	font-size: 20px;
	
} 
#center{
text-align: center;
}

#class{
font-size: 40px;
}

#classes{
text-align: center;
}

#code{
font-size: 30px;
text-align: center;
}


#head{
font-size: 60px;;
text-align: center;

}
 
 
 </style>
 <h2 id = "head"> My Classes: </h2>
 <div id = "classes">
<?php

$teacherClasses = get_classes_for_teacher($pdo, $_SESSION['key']);
foreach($teacherClasses as $classArray)
{
    $className = getClassName($pdo, $classArray['ClassId']);
    $inviteCode = $className[0]['InviteCode'];
    echo "<a id = class href=sectionSelection.php?id=".$classArray['CourseId']."&classID=".$classArray['ClassId'].">".$className[0]['Name']."</a> <p id = code>Invite Code: " . $inviteCode."</p>";
    ?>
    <br>
    <?php
}
?>
 </div>
<p id = "center"> <input id="course" type="button" value="Course Selection Page" onclick="window.location.href='courseSelection.php'" /> 
			
 
 
<?php
include 'footer.php';
?>