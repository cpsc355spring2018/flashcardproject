<?php
$pageTitle = "Quiz: ";
include 'header.php';
include 'webFunctions.php';
?>

<?php
if(!isset($_SESSION['key']))
{
    $_SESSION['error'] = 'You need to be logged in to take a quiz.';
    header("Location: loginHtml.php");
    exit();
}
$sectionID = $_GET['id'];
$classID = $_GET['classID'];
$courseID = $_GET['courseID'];
$examArray= get_section_exams_active($pdo, $sectionID);
$size = (count($examArray)-1);
$rand = rand(0,$size);
$examID = $examArray[$rand]['SectionExamId'];
$allQuestions = get_exam_questions($pdo, $examID);
$time = getClassName($pdo, $classID);

$limit = $time[0]['TimeLimitSeconds'];
?>


<style>

#demo{
text-align: center;
}


#takeExam{
	top: 30%;
	left: 0;
	right: 0;
	margin: 0 auto;
	width: 35%;
	height: 250px;

}

#Exam{

font-size: 40px;
  color: #d0d0e1;
  padding: 10px;


}
#g{
width: 100%;
background: yellow;
}

</style>

<h1 id="demo"></h1>

<script>


// Update the count down every 1 second
var limit = <?php   echo $limit ?> *1000;
var limCopy = limit;
var colorMan = limit;
var x = setInterval(function() {

  
   
    limit = limit - 1000;
    colorMan = limCopy/limit;
    
    // Time calculations for minutes and seconds
 
    var minutes = Math.floor((limit % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((limit % (1000 * 60)) / 1000);
    
    // Output the result in an element with id="demo"
    document.getElementById("demo").innerHTML ="Time Remaining: "  + minutes + "m " + seconds + "s ";
    
     if (colorMan < 2) {
    	document.getElementById("demo").style.color = "#25ffa8";
      }
     else if (colorMan >= 2 && colorMan < 4) {
      	document.getElementById("demo").style.color = "yellow";
      }
      else {
     	document.getElementById("demo").style.color = "red";
    }
    if (limit < 1000){
    	clearInterval(x);
    	document.getElementById("takeExam").submit();
    }

    
}, 1000);
</script>
<form id='takeExam' action='gradeQuiz.php?id=<?php echo $sectionID;?>&classID=<?php echo $classID;?>&courseID=<?php echo $courseID;?>' method='post' accept-charset='UTF-8'>
		<fieldset>
			<legend id = "Exam">Welcome to Exam <?php echo $examID?>:</legend>
			<input type = 'hidden' name='timeStarted' value='<?php echo time();?>'/>
			<input type = 'hidden' name = 'examID' value = '<?php echo $examID;?>'/>
			<input type = 'hidden' name = 'classID' value = '<?php echo $_GET['classID'];?>'/>
			<div id = "test">
			<?php 
			foreach ($allQuestions as $displayExam) { ?>
			<hr>
			<label for='<?php $displayExam['ExamQuestionId']?>'> <?php echo $displayExam['Question']?> </label> 
			<!--Below, for = echo exam answer and value = echo primary 
			key of answer. All goes in a nested for each loop-->
			<br>
			<?php
			//echo $displayExam['ExamQuestionId'];
			$allAnswers = getQuestionAnswers($pdo, $displayExam['ExamQuestionId']);
			//var_dump($allAnswers);?>
			<br>
			<?php 
			foreach($allAnswers as $displayChoices) {?>
			<input type='radio' name="answer<?php echo $displayExam['ExamQuestionId']?>" id="<?php echo $displayChoices['ExamQuestionAnswersId']?>" value = "<?php echo $displayChoices['Answer']?>" />
			<label for = "<?php echo $displayChoices['ExamQuestionAnswersId']?>"> <?php echo $displayChoices['Answer']?> </label>
			<br>
			
			<?php }
			} ?>
			<br>
			<input id = "g" type = 'submit' name = 'Submit' value = 'Submit'>
		</fieldset>

</form>
</div> 
<?php
include 'footer.php';
?>