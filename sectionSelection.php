<?php
$pageTitle = "Section Selection";
include 'header.php';
include 'webFunctions.php';

if(isset($_SESSION['quiz']))
{
    echo '<h2>'.$_SESSION['quiz'].'</h2>';
    unset($_SESSION['quiz']);
}
if(isset($_SESSION['key'])){
?>
<style>
#content {
	top: 30%;
	left: 0;
	right: 0;
	margin: 0 auto;
	width: 35%;
	height: 250px;
}

}
#newContent {
	padding-top: 50px;
}

#e {
	color: #d0d0e1;
}

#abc {
	color: #d0d0e1;
}

#createB {
	color: Yellow;
}

#createB:hover {
	color: #d0d0e1;
}

#Legend {
	font-size: 40px;
	color: #d0d0e1;
	padding: 10px;
}

#s {
	width: 100%;
	background: yellow;
}


#course{
	background: yellow;
	width: 25%;
	height: 40px;
	font-size: 20px;
	
} 
#center{
text-align: center;
}


#add {
	width: 100%;
}
</style>

<div id="text">
	<?php
$courseID = $_GET['id'];
$sectionArray = get_class_sections($pdo, $courseID);
if (isset($_GET['classID']))
{
    $classID = $_GET['classID'];
    $className = getClassName($pdo, $classID);
    echo ("<h1>" . $className[0]['Name']. " Sections: </h1>");
    foreach ($sectionArray as $allSections) 
    {
        echo "<a href=flashcards.php?id=" . $allSections['SectionId'] . "&classID=" . $classID . "&courseID=" . $courseID . ">" . $allSections['Name'] . "<br></a>";
    }
}
else
{
    foreach ($sectionArray as $allSections) 
    {
        echo "<a href=flashcards.php?id=" . $allSections['SectionId'] .">" . $allSections['Name'] . "<br></a>";
    }
}
?>
	</div>
<?php 
$time = getClassName($pdo, $classID);
$teacher = $time[0]['TeacherId'];
if(($_SESSION['key'] == $teacher)){
		?>
<p id = "center"> <input id="course" type="button" value="Gradebook" onclick="window.location.href='Gradebook.php?classID=<?php echo $classID?>'" /> </p>

<?php
}
if (isset($_SESSION['admin'])) {
    
    ?>



<div id="content">
	<form id='newContent' action='addContent.php?id=section' method='post'
		accept-charset='UTF-8'>
		<fieldset>
			<legend id="legend">Add Section</legend>
			<input type = 'hidden' name = 'courseID' value = '<?php echo $_GET['id'];?>'/>
			<label id="e">Section:</label> 
			<input type='text' name = 'sectionName' id='add' maxlength="100" />
			<br> <br> <input type='submit' name='Submit'
				value='Add Section' id="s" />

		</fieldset>
	</form>
</div>

 <?php }
include 'footer.php';
}
else{
    header("Location: index.php");
    exit();
}
?>
