<?php
$pageTitle = "Login";
include 'header.php';
include 'webFunctions.php';
?>
<style>

#signin {
 position: absolute;
    top: 30%;    
    left:0;
    right:0;
    margin:0 auto;
    width: 25%;
    height: 500px;
  
}

#login{
  padding-top: 50px;
  
  
}
#e{
  color: #d0d0e1;
}

#abc{
color: #d0d0e1;

}
#createB{

color: Yellow;
}
#createB:hover{
  color: #d0d0e1;
  
}

#Legend{
  font-size: 40px;
  color: #d0d0e1;
  padding: 10px;
}

#email,#password{
width: 80%;
}
#forgot,#s{
  width: 49%;
}
</style>
	<?php
	if(isset($_SESSION['error']))
	{
	    echo '<h2>'.$_SESSION['error'].'</h2>';
		unset($_SESSION['error']);
	}
	if(!isset($_SESSION['key']))
	{
	    ?>
	    <div id = "signin">
	    <form id='login' action='login.php' method='post' accept-charset='UTF-8'>
        <fieldset >
                <legend id = "legend">Login</legend>
                <input type='hidden' name='submitted' id='submitted' value='1'/>
                <label for='email' id = "e">Email: &nbsp; &nbsp; &nbsp;</label>
                <input type='text' name='email' id='email'  maxlength="100" /><br><br>
                <label for='password'id = "e" >Password:</label>
                <input type='password' name='password' id='password' maxlength="50" />
                <br><br>
                <input type='submit' name='Submit' value='Submit' id ="s" />
                <input type ="button" name = 'ForgotPassword' value = 'Forgot Password'id ="forgot"/>
                <br>
                <label for = 'create' id = "abc"> New user? Create your account here: </label>
                <a href="createAccountHtml.php" class="button" id = "createB">Create Account</a>
        </fieldset>
		</form>
  		</div>
  		<script>
  		var btn = document.getElementById('forgot');
    	btn.addEventListener('click', function() {
      	document.location.href = 'ForgotPassword.php';
    	});
  		</script>
  		<?php
	}
	else
	{
	    echo("<h2>You are already logged in. Log out to end your session or to log into another account.</h2>");
	}
include 'footer.php';
?>
