<?php
$pageTitle = "Add Content";
include 'header.php';
include 'webFunctions.php';
?>
<style>
#content {
	top: 30%;
	left: 0;
	right: 0;
	margin: 0 auto;
	width: 35%;
	height: 250px;
}

}
#login {
	padding-top: 50px;
}

#e {
	color: #d0d0e1;
}

#abc {
	color: #d0d0e1;
}

#createB {
	color: Yellow;
}

#createB:hover {
	color: #d0d0e1;
}

#Legend {
	font-size: 40px;
	color: #d0d0e1;
	padding: 10px;
}

#add, #drop {
	width: 100%;
}

#s {
	width: 100%;
	background: yellow;
}
#b1 {
	width: 49.5%;
	background: yellow;
}
</style>

<?php
if(!isset($_SESSION['admin']))
{
    $_SESSION['error'] = 'You must be an admin to add new content.';
    header("Location: loginHtml.php");
    exit();
}
else
{
    $addID = $_GET['id'];
    if($addID == "course")
    {
        $courseName = $_POST['courseName'];
        $courseDescription = $_POST['courseDescription'];
        create_course($pdo, $courseName, $courseDescription);
        echo ("New course successfully added!");
    }
    else if ($addID == "section")
    {
        $courseID = $_POST['courseID'];
        $sectionName = $_POST['sectionName'];
        create_class_section($pdo, $courseID, $sectionName);
        echo ("New section created successfully added!");
    }
    else if ($addID == "flashcard")
    {
        $sectionID = $_POST['sectionID'];
        $flashcardFront = $_POST['flashcardFront'];
        $flashcardBack = $_POST['flashcardBack'];
        create_flashcard($pdo, $sectionID, $flashcardFront, $flashcardBack);
        echo ("New flashcard successfully added!");
    }
    else if ($addID == "quiz")
    {
        $sectionID = $_POST['sectionID'];
        $isActive = $_POST['isActive'];
        createQuiz($pdo, $sectionID, $isActive);
        echo ("Quiz structure successfully added!");
    }
    else if ($addID == "qq")
    {
        $examID = $_POST['examID'];
        $question = $_POST['qText'];
        $aText = $_POST['aText'];
        $bText = $_POST['bText'];
        $cText = $_POST['cText'];
        $dText = $_POST['dText'];
        $a = $_POST['answerA'];
        $b = $_POST['answerB'];
        $c = $_POST['answerC'];
        $d = $_POST['answerD'];
        
        
        addExamQuestion($pdo, $examID, $question);
        $qID = getQuestionID($pdo);
        
        addExamAnswer($pdo, $qID[0], $aText, $a);
        addExamAnswer($pdo, $qID[0], $bText, $b);
        addExamAnswer($pdo, $qID[0], $cText, $c);
        addExamAnswer($pdo, $qID[0], $dText, $d);
        echo ("Quiz question successfully added!");
    }
    else
    {
        $_SESSION['error'] = 'An unknown error has occured.';
        header("Location: courseSelection.php");
        exit();
    }
}
include 'footer.php';
?>
