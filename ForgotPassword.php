<?php
$pageTitle = "Forgot Password";
include 'header.php';
include 'webFunctions.php';

if (isset($_SESSION['error'])) {
    echo '<h2>' . $_SESSION['error'] . '</h2>';
    unset($_SESSION['error']);
}
?>
<style>
#signin {
 position: absolute;
    top: 30%;    
    left:0;
    right:0;
    margin:0 auto;
    width: 25%;
    height: 500px;
  
}

#login{
  padding-top: 50px;
  
  
}

#e{
  color: #d0d0e1;
}

#abc{
color: #d0d0e1;

}
#createB{

color: Yellow;
}
#createB:hover{
  color: #d0d0e1;
  
}

#Legend{
  font-size: 40px;
  color: #d0d0e1;
  padding: 10px;
}

#email{
width: 80%;
}</style>

<div id="signin">
	<form id='login' action='login.php' method='post'
		accept-charset='UTF-8'>
		<fieldset>
			<legend id="legend">Forgot Password</legend>
			<input type='hidden' name='submitted' id='submitted' value='1' /> <label
				for='email' id="e">Email: &nbsp; &nbsp; &nbsp;</label> <input
				type='text' name='email' id='email' maxlength="100" /><br>
			<br> <label for='create' id="abc"> New user? Create your account
				here: </label> <a href="createAccountHtml.php" class="button"
				id="createB">Create Account</a>
		</fieldset>
	</form>
</div>

<?php
include 'footer.php';
?>
