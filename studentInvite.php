<html>
<?php
$pageTitle = "Student Invite";
include "header.php";
if(!isset($_SESSION['key']))
{
    $_SESSION['error'] = "You cannnot invite students to a class unless you're logged in.";
    header("Location: loginHtml.php");
    exit();
}
else if(!isset($_GET['id']))
{
    $_SESSION['error'] = "You must create a new class before you can invite students.";
    header("Location: courseSelection.php");
    exit();
}
else
{
    echo "Send this invite code to the students you would like to join the class: ";
    ?>
    <br>
    <?php 
    echo $_GET['id'];
    ?>
    <br>
    <?php
    echo "Save the code to invite students in the future.";
}
?>
<form>
	<input type = "button" value = "Return to Course Selection" onclick="window.location.href='courseSelection.php'" />
</form>
</html>