<?php
include 'webFunctions.php';

session_start();
$firstName = $_POST['firstName'];
$lastName = $_POST['lastName'];
$email = $_POST['email'];
$password = $_POST['password'];
$passwordConfirm = $_POST['confirmPassword'];

if ($_POST['firstName'] == "" || $_POST['lastName'] == "" || $_POST['email'] == "" || $_POST['password'] == "" || $_POST['confirmPassword'] == "") 
{
    $_SESSION['error'] = 'Not all fields were filled in, please try again.';
    header("Location: createAccountHtml.php");
    exit();
}
else if ($password != $passwordConfirm) 
{
    $_SESSION['error'] = 'Passwords do not match, please try again.';
    header("Location: createAccountHtml.php");
    exit();
} 
else if (! filter_var($email, FILTER_VALIDATE_EMAIL)) 
{
    $_SESSION['error'] = "$email is not a valid email address, please try again.";
    header("Location: createAccountHtml.php");
    exit();
} 
else if (userExists($pdo, $email)) 
{
    $_SESSION['error'] = "An account has already been created with this email, please try again.";
    header("Location: createAccountHtml.php");
    exit();
} 
else 
{
    $out = create_account($pdo, $email, $password, $firstName, $lastName);
    $bool = login_credentials($pdo, $email, $password);
    session_regenerate_id (true);
    $_SESSION['key'] = $out['UserId'];
    header("Location: courseSelection.php");
}
?>