-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 11, 2018 at 06:18 PM
-- Server version: 5.6.35
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `flashcards`
--

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `ClassId` int(11) NOT NULL,
  `TeacherId` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `TimeLimitSeconds` smallint(6) NOT NULL,
  `CourseId` int(11) NOT NULL,
  `IsActive` tinyint(1) NOT NULL,
  `InviteCode` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`ClassId`, `TeacherId`, `Name`, `TimeLimitSeconds`, `CourseId`, `IsActive`, `InviteCode`) VALUES
(8, 1, 'Example Class (references Alg 1)', 100, 1, 1, '1518194024'),
(9, 22, 'Class1', 60, 1, 0, 'invite22/1523034258');

-- --------------------------------------------------------

--
-- Table structure for table `classstudents`
--

CREATE TABLE `classstudents` (
  `ClassId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `isApproved` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `classstudents`
--

INSERT INTO `classstudents` (`ClassId`, `UserId`, `isApproved`) VALUES
(9, 22, 1);

-- --------------------------------------------------------

--
-- Table structure for table `classteachers`
--

CREATE TABLE `classteachers` (
  `ClassId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `CourseId` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`CourseId`, `Name`, `Description`) VALUES
(1, 'Algebra 2', 'Example course');

-- --------------------------------------------------------

--
-- Table structure for table `examattemptanswers`
--

CREATE TABLE `examattemptanswers` (
  `ExamAttemptAnswerId` int(11) NOT NULL,
  `ExamAttemptId` int(11) NOT NULL,
  `ExamQuestionId` int(11) NOT NULL,
  `ExamQuestionAnswerId` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `examattemptanswers`
--

INSERT INTO `examattemptanswers` (`ExamAttemptAnswerId`, `ExamAttemptId`, `ExamQuestionId`, `ExamQuestionAnswerId`) VALUES
(4, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `examattempts`
--

CREATE TABLE `examattempts` (
  `ExamAttemptId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `SectionExamId` int(11) NOT NULL,
  `StartTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DoneTime` timestamp NULL DEFAULT NULL,
  `classID` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `examattempts`
--

INSERT INTO `examattempts` (`ExamAttemptId`, `UserId`, `SectionExamId`, `StartTime`, `DoneTime`, `classID`) VALUES
(1, 1, 1, '2018-02-10 00:24:45', NULL, 0),
(2, 1, 1, '2018-02-10 00:26:56', NULL, 0),
(3, 1, 1, '2018-02-10 00:27:25', NULL, 0),
(4, 1, 1, '2018-02-10 00:27:29', NULL, 0),
(5, 1, 1, '2018-02-10 00:27:32', NULL, 0),
(6, 1, 1, '2018-02-10 00:27:36', NULL, 0),
(7, 1, 1, '2018-02-10 00:34:53', NULL, 0),
(8, 1, 1, '2018-02-10 00:35:09', NULL, 0),
(9, 1, 1, '2018-02-10 00:35:45', NULL, 0),
(10, 1, 1, '2018-03-02 15:56:48', NULL, 0),
(11, 1, 1, '2018-03-02 16:00:46', NULL, 0),
(12, 22, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(13, 22, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `examquestionanswers`
--

CREATE TABLE `examquestionanswers` (
  `ExamQuestionAnswersId` int(11) NOT NULL,
  `ExamQuestionId` int(11) NOT NULL,
  `Answer` mediumtext,
  `Correct` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `examquestionanswers`
--

INSERT INTO `examquestionanswers` (`ExamQuestionAnswersId`, `ExamQuestionId`, `Answer`, `Correct`) VALUES
(1, 1, 'Mr. Legit', 1);

-- --------------------------------------------------------

--
-- Table structure for table `examquestions`
--

CREATE TABLE `examquestions` (
  `ExamQuestionId` int(11) NOT NULL,
  `SectionExamId` int(11) NOT NULL,
  `Question` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `examquestions`
--

INSERT INTO `examquestions` (`ExamQuestionId`, `SectionExamId`, `Question`) VALUES
(1, 1, 'Who is legit?');

-- --------------------------------------------------------

--
-- Table structure for table `flashcards`
--

CREATE TABLE `flashcards` (
  `FlashCardId` int(11) NOT NULL,
  `SectionId` int(11) NOT NULL,
  `Front` mediumtext NOT NULL,
  `Back` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `flashcards`
--

INSERT INTO `flashcards` (`FlashCardId`, `SectionId`, `Front`, `Back`) VALUES
(2, 1, 'Which items below represent an equation? <br /> `m^2=3` <br /> `-4xy^2` <br /> `x-125`', '`m^2 =3` <br />\r\nExplanation: An equation is a mathematical\r\nsentence. An equation has a left and right side\r\nseparated by an equal sign.'),
(4, 1, 'Which items below represent an expression? <br />\r\n`x+2=5` <br />\r\n`4xy^2` <br />\r\n`y^2=5` <br />\r\n`45(x + y)` <br />\r\n`6(3+2)=30` <br /> ', '`4xy^2` <br /> `45(x + y)` <br />\r\nExplanation: An expression is a mathematical phrase containing numbers, variables,\r\nexponents, parentheses and operations.');

-- --------------------------------------------------------

--
-- Table structure for table `sectionexams`
--

CREATE TABLE `sectionexams` (
  `SectionExamId` int(11) NOT NULL,
  `SectionId` int(11) NOT NULL,
  `IsActive` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sectionexams`
--

INSERT INTO `sectionexams` (`SectionExamId`, `SectionId`, `IsActive`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `SectionId` int(11) NOT NULL,
  `CourseId` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`SectionId`, `CourseId`, `Name`) VALUES
(1, 1, 'Section 1.1'),
(2, 1, 'Section 1.2');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `UserId` int(11) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Prefix` varchar(10) DEFAULT NULL,
  `FirstName` varchar(50) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `IsAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `IsActive` tinyint(1) NOT NULL DEFAULT '1',
  `PasswordRecoveryCode` char(10) DEFAULT NULL,
  `IsVerrified` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`UserId`, `Email`, `Password`, `Prefix`, `FirstName`, `LastName`, `IsAdmin`, `IsActive`, `PasswordRecoveryCode`, `IsVerrified`) VALUES
(1, 'test', '$2y$10$j2oI6jHi.GCG68LDdfJ0IOLMtt8Xh0YR8ROmYndplVXgA6Uw2v/.O', NULL, 'test', 'test', 0, 1, NULL, 0),
(2, 'test', '$2y$10$r660qOn171hBWI0zuguk6.tPS7QfPSJuBnKpwcaGxKV3bjhp44uEi', NULL, 'test', 'test', 0, 1, NULL, 0),
(3, 'test', '$2y$10$nG5G/SZPHM/T1bDgqaeMouwbrUaNk8A2eCjVTqDfjASJzNm4r/OXW', NULL, 'test', 'test', 0, 1, NULL, 0),
(4, 'test', '$2y$10$nv8/zPg80TU5BtmxIjLfTe/0W7XakqKqmXBUjtt86RQbTo.eVKw9K', NULL, 'test', 'test', 0, 1, NULL, 0),
(5, 'test', '$2y$10$pQThVHPUzkwyG3hWGG8XN.Cf8HFj3.5xqO/lDcSlMGp/kGNVegc6G', NULL, 'test', 'test', 0, 1, NULL, 0),
(6, 'test', '$2y$10$2zAHgW5BU089m0xXvsE5yuBURZpRmGNrxiEC0Ici5Ljs0QleKcrNC', NULL, 'test', 'test', 0, 1, NULL, 0),
(7, 'test', '$2y$10$fAAgIQniRUspN3JrU4GhHOPUxPRiBT9gzTS/q477BIVXJlIqpBiLC', NULL, 'test', 'test', 0, 1, NULL, 0),
(8, 'test', '$2y$10$yGx0OaHn8d9GOanwA29IoevWOSydE94OtCuiRGN/oYa82uzAurKrG', NULL, 'test', 'test', 0, 1, NULL, 0),
(9, 'test', '$2y$10$2Ywo3dExxKpIo9RC24rfMuOF2t1p/KY8B2b4POC2jmxpFXJuyS0ki', NULL, 'test', 'test', 0, 1, NULL, 0),
(10, 'test', '$2y$10$45RLmzNCdMzEVq38XUwPR.YhjWFFGYiuEjIpE8o1z//clnD0VFXya', NULL, 'test', 'test', 0, 1, NULL, 0),
(11, 'test', '$2y$10$jQlpdj98j3W6ScHVHWe5a.CjEYP49JEaSgZcMKhFhnFe7/x8kbyTS', NULL, 'test', 'test', 0, 1, NULL, 0),
(12, 'test', '$2y$10$6zuLtYMuBENIKoGYcFmosOksaingPL4VjxTFZP5DS2.GFBF4XgoUm', NULL, 'test', 'test', 0, 1, NULL, 0),
(13, 'test', '$2y$10$ZRN3uraS5WemZXtr7j1RqeCiRurysf2qCA4NWjlsK1goQbB.IB7cu', NULL, 'test', 'test', 0, 1, NULL, 0),
(14, 'test', '$2y$10$4eGuncehKtqKh5FMTmQmKeQC8JPosA/HsTumsTCdolNg9xV1W9zya', NULL, 'test', 'test', 0, 1, NULL, 0),
(15, 'test', '$2y$10$ldrKJTZm6ZcQ2d.dexOvQOPSccBMb5gqLGs6kMtEZOBDNwHaadqke', NULL, 'test', 'test', 0, 1, NULL, 0),
(16, 'test', '$2y$10$EaQGaQHeUunuB15CtyaOlunWj46ugaX80MQsV9nI4KjZhQZM41RUy', NULL, 'test', 'test', 0, 1, NULL, 0),
(17, 'test', '$2y$10$cffKIPCx.KYfGrz.E6BKJOtA8v6kvQ2SqUq0i6FfCW.4K0TOY5eA6', NULL, 'test', 'test', 0, 1, NULL, 0),
(18, 'test', '$2y$10$5a6RYyl.yAzo7.9sM648guNeJxsLkif3//MC/TQxoP95Cf4pOWQIm', NULL, 'test', 'test', 0, 1, NULL, 0),
(19, 'test', '$2y$10$X0VKDjQ8UHeTiuxHb5JgkeLBLmPn25aPyKNQCcn/EsTDv7ePm6B2O', NULL, 'test', 'test', 0, 1, NULL, 0),
(20, 'test', '$2y$10$UFZYYiAqTZBVaeyBwAMiz.UVNkjU.XCETa3uzHT7slag1.DBB2MTO', NULL, 'test', 'test', 0, 1, NULL, 0),
(21, 'test', '$2y$10$8fGVKOxq11QWgZ5/Vzpf4enu33fJGzR1kZMCjmYhIfmI0d6DiCAZ2', NULL, 'test', 'test', 0, 1, NULL, 0),
(22, 'TheWonton@yahoo.com', '$2y$10$wrGNGsuqIU3MsvTJYfC6WOQ/KTs2YgExrcbnKiZj4OVNJI.6AoeJS', NULL, 'Juan', 'Dunlap', 0, 1, NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`ClassId`),
  ADD KEY `CourseId` (`CourseId`),
  ADD KEY `TeacherId` (`TeacherId`);

--
-- Indexes for table `classstudents`
--
ALTER TABLE `classstudents`
  ADD PRIMARY KEY (`ClassId`,`UserId`),
  ADD KEY `UserId` (`UserId`);

--
-- Indexes for table `classteachers`
--
ALTER TABLE `classteachers`
  ADD PRIMARY KEY (`ClassId`,`UserId`),
  ADD KEY `UserId` (`UserId`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`CourseId`);

--
-- Indexes for table `examattemptanswers`
--
ALTER TABLE `examattemptanswers`
  ADD PRIMARY KEY (`ExamAttemptAnswerId`),
  ADD KEY `ExamAttemptId` (`ExamAttemptId`),
  ADD KEY `ExamQuestionId` (`ExamQuestionId`),
  ADD KEY `examattemptanswers_ibfk_3` (`ExamQuestionAnswerId`);

--
-- Indexes for table `examattempts`
--
ALTER TABLE `examattempts`
  ADD PRIMARY KEY (`ExamAttemptId`),
  ADD KEY `SectionExamId` (`SectionExamId`),
  ADD KEY `UserId` (`UserId`);

--
-- Indexes for table `examquestionanswers`
--
ALTER TABLE `examquestionanswers`
  ADD PRIMARY KEY (`ExamQuestionAnswersId`),
  ADD KEY `ExamQuestionId` (`ExamQuestionId`);

--
-- Indexes for table `examquestions`
--
ALTER TABLE `examquestions`
  ADD PRIMARY KEY (`ExamQuestionId`),
  ADD KEY `SectionExamId` (`SectionExamId`);

--
-- Indexes for table `flashcards`
--
ALTER TABLE `flashcards`
  ADD PRIMARY KEY (`FlashCardId`),
  ADD KEY `SectionId` (`SectionId`);

--
-- Indexes for table `sectionexams`
--
ALTER TABLE `sectionexams`
  ADD PRIMARY KEY (`SectionExamId`),
  ADD KEY `SectionId` (`SectionId`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`SectionId`),
  ADD KEY `CourseId` (`CourseId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`UserId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `ClassId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `CourseId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `examattemptanswers`
--
ALTER TABLE `examattemptanswers`
  MODIFY `ExamAttemptAnswerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `examattempts`
--
ALTER TABLE `examattempts`
  MODIFY `ExamAttemptId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `examquestionanswers`
--
ALTER TABLE `examquestionanswers`
  MODIFY `ExamQuestionAnswersId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `examquestions`
--
ALTER TABLE `examquestions`
  MODIFY `ExamQuestionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `flashcards`
--
ALTER TABLE `flashcards`
  MODIFY `FlashCardId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sectionexams`
--
ALTER TABLE `sectionexams`
  MODIFY `SectionExamId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `SectionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `UserId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `classes`
--
ALTER TABLE `classes`
  ADD CONSTRAINT `classes_ibfk_1` FOREIGN KEY (`CourseId`) REFERENCES `courses` (`CourseId`),
  ADD CONSTRAINT `classes_ibfk_2` FOREIGN KEY (`TeacherId`) REFERENCES `users` (`UserId`);

--
-- Constraints for table `classstudents`
--
ALTER TABLE `classstudents`
  ADD CONSTRAINT `classstudents_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`),
  ADD CONSTRAINT `classstudents_ibfk_2` FOREIGN KEY (`ClassId`) REFERENCES `classes` (`ClassId`);

--
-- Constraints for table `classteachers`
--
ALTER TABLE `classteachers`
  ADD CONSTRAINT `classteachers_ibfk_1` FOREIGN KEY (`ClassId`) REFERENCES `classes` (`ClassId`),
  ADD CONSTRAINT `classteachers_ibfk_2` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`);

--
-- Constraints for table `examattemptanswers`
--
ALTER TABLE `examattemptanswers`
  ADD CONSTRAINT `examattemptanswers_ibfk_1` FOREIGN KEY (`ExamAttemptId`) REFERENCES `examattempts` (`ExamAttemptId`),
  ADD CONSTRAINT `examattemptanswers_ibfk_2` FOREIGN KEY (`ExamQuestionId`) REFERENCES `examquestions` (`ExamQuestionId`),
  ADD CONSTRAINT `examattemptanswers_ibfk_3` FOREIGN KEY (`ExamQuestionAnswerId`) REFERENCES `examquestionanswers` (`ExamQuestionAnswersId`);

--
-- Constraints for table `examattempts`
--
ALTER TABLE `examattempts`
  ADD CONSTRAINT `examattempts_ibfk_1` FOREIGN KEY (`SectionExamId`) REFERENCES `sectionexams` (`SectionExamId`),
  ADD CONSTRAINT `examattempts_ibfk_2` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`);

--
-- Constraints for table `examquestionanswers`
--
ALTER TABLE `examquestionanswers`
  ADD CONSTRAINT `examquestionanswers_ibfk_1` FOREIGN KEY (`ExamQuestionId`) REFERENCES `examquestions` (`ExamQuestionId`);

--
-- Constraints for table `examquestions`
--
ALTER TABLE `examquestions`
  ADD CONSTRAINT `examquestions_ibfk_1` FOREIGN KEY (`SectionExamId`) REFERENCES `sectionexams` (`SectionExamId`);

--
-- Constraints for table `flashcards`
--
ALTER TABLE `flashcards`
  ADD CONSTRAINT `flashcards_ibfk_1` FOREIGN KEY (`SectionId`) REFERENCES `sections` (`SectionId`);

--
-- Constraints for table `sectionexams`
--
ALTER TABLE `sectionexams`
  ADD CONSTRAINT `sectionexams_ibfk_1` FOREIGN KEY (`SectionId`) REFERENCES `sections` (`SectionId`);

--
-- Constraints for table `sections`
--
ALTER TABLE `sections`
  ADD CONSTRAINT `sections_ibfk_1` FOREIGN KEY (`CourseId`) REFERENCES `courses` (`CourseId`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
