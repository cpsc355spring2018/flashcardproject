<?php
$pageTitle = "Course Selection";
include 'header.php';
include 'webFunctions.php';
?>
	<?php
	if(isset($_SESSION['error']))
	{
	    echo '<h2>'.$_SESSION['error'].'</h2>';
		unset($_SESSION['error']);
	}
	?>

<style>
#content {
	top: 30%;
	left: 0;
	right: 0;
	margin: 0 auto;
	width: 35%;
	height: 250px;
}

}
#newContent {
	padding-top: 50px;
}

#e {
	color: #d0d0e1;
}

#abc {
	color: #d0d0e1;
}

#createB {
	color: Yellow;
}

#createB:hover {
	color: #d0d0e1;
}

#Legend {
	font-size: 40px;
	color: #d0d0e1;
	padding: 10px;
}

#s {
	width: 100%;
	background: yellow;
}

#add,#description {
	width: 100%;
}
</style>

<div id = "text">
	<h1>Select a course to study:</h1>
	<?php
	$courseArray = get_all_courses($pdo);
	//this link sets the CourseId as the end of the web address so it can be pulled later for use.
	foreach($courseArray as $allCourses)
	{ 
	    echo "<a href=courseInfo.php?id=".$allCourses['CourseId'].">".$allCourses['Name']."</a>";
	}
	?>
	</div>



<?php 

 if (isset($_SESSION['admin'])){

?>



<div id="content">
	<form id='newContent' action='addContent.php?id=course' method='post'
		accept-charset='UTF-8'>
		<fieldset>
			<legend id="legend">Add Course</legend>
			
			 <label id="e"> Course Name:</label> 
			 <input type='text' name = 'courseName' id='add' maxlength="100" /><br> <br> 

			 <label id="e"> Course Description:</label> 
			 <br>
			 <input type='text' id='description' maxlength="100" /><br> <br> 
			 
			 <input type='submit' name = 'courseDescription' name='Submit' value='Add Course' id="s" />

		</fieldset>
	</form>
</div>

<?php 

 }

?>



<?php
include 'footer.php';
?>
