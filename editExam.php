<?php
$pageTitle = "Admin Quiz";
include 'header.php';
include 'webFunctions.php';
$examID = $_GET['examID'];
$exam = get_exam_questions($pdo, $examID);
?>
<style>

#content {
top: 30%;
left: 0;
right: 0;
margin: 0 auto;
width: 35%;
height: 250px;
}

}
#login {
padding-top: 50px;
}


#createB {
color: Yellow;
}

#createB:hover,#e {
color: #d0d0e1;
}

#Legend {
font-size: 40px;
color: #d0d0e1;
padding: 10px;
}

#add, #drop {
width: 100%;
}

#s {
width: 100%;
background: yellow;
}
#b1 {
width: 100%;
background: yellow;
}


</style>
<h1> Exam <?php echo $examID;?> Questions and Answers: </h1>
<?php
foreach ($exam as $examQuestions)
{
    echo ( "<h2>Question " . $examQuestions['ExamQuestionId'] . "</h2>" );
    echo ($examQuestions['Question']);
    ?>
    <br>
    <?php 
    $answers = getQuestionAnswers($pdo, $examQuestions['ExamQuestionId']);
    echo ("<h2> Question ". $examQuestions['ExamQuestionId'] ." Answers </h2>");
    foreach ($answers as $questionsAnswers)
    {
        echo ($questionsAnswers['Answer']);
        if ($questionsAnswers['Correct'] == 1)
        {
            echo (" = Correct");
        }
        ?>
        <br>
        <?php
    }
}
if (isset($_SESSION['admin'])) {
	?>



<div id="content">
	<form id='login' action='addContent.php?id=qq' method='post'
		accept-charset='UTF-8'>
		
		<fieldset>
		
			<legend id="legend">Add Quiz Answers/Questions</legend>
			
				
			 <label  id="e"> Question text: </label> <input name = "qText" type='text' id='add' maxlength="250" /><br> <br> 
			
			 <label  id="e"> A Answer: </label> <input name = "aText" type='text' id='add' maxlength="250" /><br> <br> 
			
			 <label  id="e"> B Answer: </label> <input name = "bText" type='text' id='add' maxlength="250" /><br> <br> 
			
			 <label  id="e"> C Answer: </label> <input name = "cText" type='text' id='add' maxlength="250" /><br> <br> 
			
			 <label  id="e"> D Answer: </label> <input name = "dText" type='text' id='add' maxlength="250" /><br> <br> 
			<input type = 'hidden' name = 'examID' value = '<?php echo($examID);?>'/>
			<input id = "rad" type="radio" name="answerA" value="A"> A<br>
 			<input id = "rad" type="radio" name="answerB" value="B"> B<br>
  			<input id = "rad" type="radio" name="answerC" value="C"> C <br>
  			<input id = "rad" type="radio" name="answerD" value="D"> D <br><br>
			<input type='submit' name = 'question' name='Submit' value='Add Question' id="b1" />
				
				
				
		</fieldset>
	</form>
</div>

 
 <?php
}
include 'footer.php';
?>