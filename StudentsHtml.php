<?php
$pageTitle = "Student Page";
include 'header.php';
include 'webFunctions.php';

if(isset($_SESSION['error']))
{
    echo '<h2>'.$_SESSION['error'].'</h2>';
    unset($_SESSION['error']);
}
if(isset($_SESSION['join']))
{
    echo '<h2>'.$_SESSION['join'].'</h2>';
    unset($_SESSION['join']);
}
?>

<style>
#a {
	position: relative;
    top: 0%;    
    left:0;
    right:0;
    margin:0 auto;
    width: 35%;
    height: 200px;
}
#class{
font-size: 40px;
}

#classes{
text-align: center;
}

#code{
font-size: 30px;
text-align: center;
}


#head{
font-size: 60px;;
text-align: center;

}

#g{
width: 100%;
}
#join{
width: 100%;
background: yellow;
}


</style>
<div id = "a">
<fieldset>
  <h1> Join Classes Here: </h1>
  <form id='joinClass' action='joinWithInvite.php' method='post' accept-charset='UTF-8'>
  <label for = 'code'> Enter Your Invite Code: </label>
  <input id = "g" type='text' name='code' id='code' maxlength="50" /> 
  <br>
  <br>
  <input id = "join" type = 'submit' name = 'Submit' value = 'Join Class'/>
  </form>
 </fieldset>
 </div>
 <h2 id = "head"> Enrolled Classes: </h2>
 <div id = "classes">
<?php

$studentClasses = get_classes_for_student($pdo, $_SESSION['key']);
foreach($studentClasses as $classArray)
{
    $className = getClassName($pdo, $classArray['ClassId']);
    //get class names from database using $classArray['ClassId'];
    //possibly make new web function
    echo "<a id = class href=sectionSelection.php?id=".$classArray['CourseId']."&classID=".$classArray['ClassId'].">".$className[0]['Name']."</a>";
    ?>
    <br>
    <?php
    
}
?>
</div>
<?php 
include 'footer.php';
?>