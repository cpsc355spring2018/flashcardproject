<?php
$pageTitle = "Create Class";
include 'header.php';
include 'webFunctions.php';
$teacherID = $_POST['teacherID'];
$className = $_POST['className'];
$timeLimit = $_POST['timeLimit'];
$courseID = $_POST['courseID'];
$inviteCode = "invite".$teacherID.time();

if ($_POST['teacherID'] == "" || $_POST['className'] == "" || $_POST['timeLimit'] == "")
{
    $_SESSION['error'] = 'Not all fields were filled in, please try again.';
    header("Location: createClassHtml.php");
    exit();
}
else
{
    create_class($pdo, $teacherID, $className, $timeLimit, $courseID, $inviteCode);
    echo("Class created successfully!");
    echo (" Invite students with this code: " . $inviteCode);
}
?>
<form>
	<input type = "button" value = "Return to Course Selection" onclick="window.location.href='courseSelection.php'" />
</form>
</html>
<?php 
include 'footer.php';
?>
