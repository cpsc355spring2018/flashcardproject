<?php
session_start();
include 'webFunctions.php';

$timeSubmitted = time();
$sectionID = $_GET['id'];
$classID = $_GET['classID'];
$courseID = $_GET['courseID'];
if(isset($_POST['Submit']))
{
    $_SESSION['quiz'] = 'Quiz completed successfully!';
    $userid = $_SESSION['key'];
    $sectionexamid = $_POST['examID']; 
    $starttime = $_POST['timeStarted'];
    $donetime = $timeSubmitted;
    new_exam_attempt($pdo, $userid, $sectionexamid, $starttime, $donetime, $classID);
}

header("Location: sectionSelection.php?id=". $courseID . "&classID=" . $classID);
?>