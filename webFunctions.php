<?php
try
{
    $pdo = new PDO('mysql:host=localhost;dbname=flashcards', 'root', 'cpsc355');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e)
{
    echo $e->getMessage();
    exit();
}

// This function will insert a row into the user table and return that row
function create_account(PDO $pdo, $email, $pass, $firstname, $lastname)
{
    $password = password_hash($pass, PASSWORD_BCRYPT);
    $sql = 'INSERT INTO users (Email, Password, FirstName, LastName) VALUES (:email, :password, :firstname, :lastname)';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam('password', $password);
    $stmt->bindParam(':firstname', $firstname);
    $stmt->bindParam(':lastname', $lastname);
    $stmt->execute();
    
    $sql = 'SELECT * FROM users WHERE Email = :email LIMIT 1';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam('email', $email);
    $stmt->execute();
    $row = $stmt->fetch();
    return ($row);
}

// This will take an email / pass and return true or false whether or not in db
function login_credentials(PDO $pdo, $email, $pass)
{
    //$password = password_hash($pass, PASSWORD_BCRYPT);
    $sql = 'SELECT * FROM users WHERE Email = :email';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':email', $email);
    // $stmt->bindParam(':password', $password);
    $stmt->execute();
    $row = $stmt->fetch();
    if (password_verify($pass, $row['Password'])) {
        return true;
    } else {
        return false;
    }
}
//checks the database to see if the email given is already in use
function userExists(PDO $pdo, $newEmail)
{
    $exist = htmlspecialchars($newEmail, ENT_QUOTES, 'UTF-8');
    $sql = 'SELECT * FROM users WHERE email = :email';
    $s = $pdo->prepare($sql);
    $s->bindValue(':email', $exist);
    $s->execute();
    
    if($s -> rowCount() > 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}
//This will create a class from a given course (blueprint)
function create_class(PDO $pdo, $teacherid, $name, $timelimitseconds, $courseid, $inviteCode)
{
    $sql = 'INSERT INTO classes (TeacherId, Name, TimeLimitSeconds, CourseId, InviteCode) VALUES (:teacherid, :name, :timelimitseconds, :courseid, :inviteCode)';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':teacherid', $teacherid);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':timelimitseconds', $timelimitseconds);
    $stmt->bindParam(':courseid', $courseid);
    $stmt->bindParam(':inviteCode', $inviteCode);
    $stmt->execute();
    
    $sql = "SELECT * FROM classes WHERE ClassId = {$pdo->lastInsertId()}";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $row = $stmt->fetch();
    return ($row);
}

//This function is used when a student requests to join a class from the class list
function join_class_waiting_list(PDO $pdo, $userid, $classid)
{
    $sql = 'INSERT INTO classstudents (UserId, ClassId) VALUES (:userid, :classid)';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':userid', $userid);
    $stmt->bindParam(':classid', $classid);
    $stmt->execute();
    
}

//This function is used to approve a student into a class
//after the student has requested to jion the class from the class list
function join_class_accept_student(PDO $pdo, $userid, $classid)
{
    $sql = 'UPDATE classstudents SET IsApproved = 1 WHERE UserId = :userid AND ClassId = :classid';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':userid', $userid);
    $stmt->bindParam(':classid', $classid);
    $stmt->execute();
}

//This function is used by a student to enter a class via invite code.
function join_class_by_code(PDO $pdo, $userid, $invitecode)
{
    $sql = 'SELECT * FROM classes WHERE InviteCode = :invitecode';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':invitecode', $invitecode);
    $stmt->execute();
    if($stmt->rowCount() == 1)
    {
        $row = $stmt->fetch();
        join_class_waiting_list($pdo, $userid, $row['ClassId']);
        join_class_accept_student($pdo, $userid, $row['ClassId']);
        return true;
        //verify student was successfully entered into course.
    }
    else
    {
        echo('Invalid invite code');
        return false;
    }
}

function student_in_course(PDO $pdo, $userid)
{
    $sql = "SELECT * FROM users WHERE UserId = :userid";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':userid', $userid);
    $stmt->execute();
    if($stmt->rowCount() == 1)
    {
        $row = $stmt->fetch();
        join_class_waiting_list($pdo, $userid, $row['ClassId']);
        join_class_accept_student($pdo, $userid, $row['ClassId']);
    }
    
}
//Returns an array of students (each student is also an araray)
function get_students_in_class(PDO $pdo, $classid)
{
    $sql = 'SELECT * FROM classstudents
    INNER JOIN users On classstudents.UserId = users.UserId
    INNER JOIN classes ON classstudents.ClassId = classes.ClassId
    WHERE classstudents.classid = :classid';
    
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':classid', $classid);
    $stmt->execute();
    
    $result = $stmt->fetchAll();
    return $result;
}

//Returns an array of classes (each class is also an araray)
function get_classes_for_student(PDO $pdo, $userid)
{
    $sql = 'SELECT * FROM classstudents
    INNER JOIN users On classstudents.UserId = users.UserId
    INNER JOIN classes ON classstudents.ClassId = classes.ClassId
    WHERE classstudents.UserId = :userid';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':userid', $userid);
    $stmt->execute();
    
    $result = $stmt->fetchAll();
    return $result;
}

function get_all_courses(PDO $pdo)
{
    $sql = 'SELECT * FROM courses';
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    
    $result = $stmt->fetchAll();
    return $result;
}


function get_all_classes(PDO $pdo)
{
    $sql = 'SELECT * FROM classes';
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    
    $result = $stmt->fetchAll();
    return $result;
}

//TODO: DISCOVERED that I have a many to many and a one to many relationship for teachers.
//NEED to decide.  This function assumes one teacher per class.
//Returns an array of classes (each class is also an araray)
function get_classes_for_teacher(PDO $pdo, $teacherid)
{
    $sql = 'SELECT * FROM classes
    INNER JOIN users ON classes.TeacherId = users.UserId
    WHERE classes.TeacherId = :teacherid';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':teacherid', $teacherid);
    $stmt->execute();
    
    $result = $stmt->fetchAll();
    return $result;
}

function get_user_info(PDO $pdo, $userid)
{
    $sql = 'SELECT * FROM users WHERE UserId = :userid';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':userid', $userid);
    $stmt->execute();
    
    return $stmt->fetch();
}

function getCourseInfo(PDO $pdo, $CourseId)
{
    $sql = 'SELECT * FROM courses WHERE CourseId = :CourseId';
    $stmt = $pdo->prepare($sql);
    $stmt -> bindParam(':CourseId', $CourseId);
    $stmt -> execute();
    $result = $stmt->fetchAll();
    return $result;
}

function getClassName(PDO $pdo, $ClassId)
{
    $sql = 'SELECT * FROM classes WHERE ClassId = :ClassId';
    $stmt = $pdo->prepare($sql);
    $stmt -> bindParam(':ClassId', $ClassId);
    $stmt -> execute();
    $result = $stmt->fetchAll();
    return $result;
}

function get_class_sections(PDO $pdo, $courseid)
{
    $sql = 'SELECT * FROM sections WHERE CourseId = :courseid';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':courseid', $courseid);
    $stmt->execute();
    
    return $stmt->fetchAll();
}

//I imagine you will have this equivalent information after
//calling get_class_sections so this may not be needed.
//(A particular section would be within all the class sections array)
function get_section_info(PDO $pdo, $sectionid)
{
    $sql = 'SELECT * FROM sections WHERE SectionId = :sectionid';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':sectionid', $sectionid);
    $stmt->execute();
    
    return $stmt->fetch();
}

function get_section_cards(PDO $pdo, $sectionid)
{
    $sql = 'SELECT * FROM flashcards WHERE SectionId = :sectionid';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':sectionid', $sectionid);
    $stmt->execute();
    
    return $stmt->fetchAll();
}

//Got past the quizing, moving onto the exams.
//TODO need to go back and make sure these have WHERE is_active = true


function get_section_exams_active(PDO $pdo, $sectionid)
{
    $sql = 'SELECT * FROM sectionexams WHERE SectionId = :sectionid';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':sectionid', $sectionid);
    $stmt->execute();
    
    return $stmt->fetchAll();
};

function get_exam_questions(PDO $pdo , $sectionexamid)
{
    $sql = 'SELECT * FROM examquestions WHERE SectionExamId = :sectionexamid';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':sectionexamid', $sectionexamid);
    $stmt->execute();
    
    return $stmt->fetchAll();
};
function get_exam_question_answer(PDO $pdo, $questionid)
{
    //TODO next database change include changing this to ExamQuestionAnswerId (not answers)
    $sql = 'SELECT * FROM examquestionanswers WHERE examquestionanswersId = :questionid';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':questionid', $questionid);
    $stmt->execute();
    
    return $stmt->fetch();
};
function getQuestionAnswers(PDO $pdo, $questionid)
{
    $sql = 'SELECT * FROM examquestionanswers WHERE ExamQuestionId = :questionid';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':questionid', $questionid);
    $stmt->execute();
    
    return $stmt->fetchAll();
};
function new_exam_attempt(PDO $pdo,$userid, $sectionexamid, $starttime, $donetime, $classID)
{
    $sql = "INSERT INTO examattempts(UserId, SectionExamId, StartTime, DoneTime, classID)
    VALUES(:userid, :sectionexamid, :starttime, :donetime, :classID)";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':userid', $userid);
    $stmt->bindParam(':sectionexamid', $sectionexamid);
    $stmt->bindParam(':starttime', $starttime);
    $stmt->bindParam(':donetime', $donetime);
    $stmt->bindParam(':classID', $classID);
    $stmt->execute();
    
    return get_exam_attempt($pdo, $pdo->lastInsertId());
    
};

function get_exam_attempt_answers(PDO $pdo, $examattemptid)
{
    $sql = 'SELECT * FROM examattemptanswers WHERE ExamAttemptId = :examattemptid';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':examattemptid', $examattemptid);
    $stmt->execute();
    
    return $stmt->fetchAll();
};

function new_exam_attempt_answer(PDO $pdo, $examattemptid, $examquestionid, $examquestionanswerid)
{
    $sql = "INSERT INTO examattemptanswers(ExamAttemptId, ExamQuestionId, ExamQuestionAnswerId)
    VALUES(:examattemptid, :examquestionid, :examquestionanswerid)";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':examattemptid', $examattemptid);
    $stmt->bindParam(':examquestionid', $examquestionid);
    $stmt->bindParam(':examquestionanswerid', $examquestionanswerid);
    $stmt->execute();

    //I don't imagine get_exam_attempt_answer is necesary because we have get_exam_attempt_answers().
    //return get_exam_attempt_answer($pdo, $pdo->lastInsertId());
}

function get_exam_attempt(PDO $pdo, $examattemptid)
{
    $sql = 'SELECT * FROM examattempts WHERE ExamAttemptId = :examattemptid';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':examattemptid', $examattemptid);
    $stmt->execute();
    
    return $stmt->fetch();
};
function getflashcards(PDO $pdo, $sectionID)
{
    $sql = 'SELECT * FROM flashcards WHERE SectionId = :sectionID';
    $stmt = $pdo -> prepare($sql);
    $stmt -> bindParam(':sectionID', $sectionID);
    $stmt -> execute();
    
    return $stmt -> fetchAll();
};
function getFlashcardFront(PDO $pdo, $flashcardID)
{
    $sql = 'SELECT * FROM flashcards WHERE FlashCardId = :flashcardID';
    $stmt = $pdo -> prepare($sql);
    $stmt -> bindParam(':flashcardID', $flashcardID);
    $stmt -> execute();
    
    return $stmt -> fetchAll();
};

//This is a function to create the course, which is the blueprint for CLASSES
//If you want a teacher to make a class, there is a function already for that.
function create_course(PDO $pdo, $name, $description)
{
    $sql = 'INSERT INTO courses (Name, Description) VALUES (:name, :description)';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':description', $description);
    $stmt->execute();
}

function create_class_section(PDO $pdo, $courseid, $name)
{
    $sql = 'INSERT INTO sections (CourseId, Name) VALUES (:courseid, :name)';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':courseid', $courseid);
    $stmt->bindParam(':name', $name);
    $stmt->execute();
}

function create_flashcard(PDO $pdo, $sectionid, $front, $back)
{
    $sql = 'INSERT INTO flashcards (SectionId, Front, Back) VALUES (:sectionid, :front, :back)';
    $stmt = $pdo->prepare($sql);
    $stmt -> bindParam(':sectionid', $sectionid);
    $stmt->bindParam(':front', $front);
    $stmt->bindParam(':back', $back);
    $stmt->execute();
}
function createQuiz(PDO $pdo, $sectionID, $isActive)
{
    $sql = 'INSERT INTO sectionexams (SectionId, IsActive) VALUES (:SectionId, :IsActive)';
    $stmt = $pdo->prepare($sql);
    $stmt -> bindParam(':SectionId', $sectionID);
    $stmt->bindParam(':IsActive', $isActive);
    $stmt->execute();
}

function is_student_in_class(PDO $pdo, $classid, $userid)
{
    $sql = 'SELECT * FROM classstudents
    WHERE classstudents.ClassId = :classid AND 
	      classstudents.UserId = :userid';
    
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':classid', $classid);
    $stmt->bindParam(':userid', $userid);
    $stmt->execute();
    
    $result = $stmt->rowCount () > 0;
    return $result;
}

function get_grade_book(PDO $pdo, $classid)
{
    $array = array();

    $sql = 'SELECT *  FROM examattempts INNER JOIN users ON examattempts.UserId = users.UserId
    WHERE ClassId = :classid
    ORDER BY SectionExamId';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':classid', $classid);
    $stmt->execute();
    $result = $stmt -> fetchAll();
    
    
    $new_array;
    //create that index 'result' incase nothing was returned
    //that way it does not throw an error when referencing result in code.
    //(because the foreach will not be executed)
    $new_array['result'] = '';
    foreach($result as $row)
    {
        //print("in foreach<br><br>");
        $examgrade = get_attempt_grade($pdo, $row['ExamAttemptId']);
       
        //Storing the exam grade as an additional column
        $row['result'] = $examgrade;

        //had to add it to new array and return new array
        //coundnt seem to change the orig.

        //clear array
        $new_array = array();
        //add column
        $new_array[] = $row;
    }

    //print_r($new_array);
    return $new_array;
}

function get_attempt_grade(PDO $pdo, $examattemptid)
{
    $correct = 0;
    $total = 0;
    $sql = 'SELECT * FROM examattemptanswers INNER JOIN examquestionanswers WHERE ExamAttemptId = :examattemptid';
    $stmt = $pdo -> prepare($sql);
    $stmt -> bindParam(':examattemptid', $examattemptid);
    $stmt -> execute();
    
    $result = $stmt -> fetchAll();

    foreach($result as $row)
    {
        if($row['Correct'] == '1')
        {
            $correct++;
        }
        $total++;
    }

    return $correct/$total;
}



function addExamQuestion(PDO $pdo, $examID, $qText)
{
	$sql = 'INSERT INTO examquestions (SectionExamId, Question) VALUES (:examID, :q)';
	$stmt = $pdo->prepare($sql);
	$stmt -> bindParam(':examID', $examID);
	$stmt->bindParam(':q', $qText);
	$stmt->execute();
}
function getQuestionID(PDO $pdo)
{
	$sql = 'SELECT MAX(ExamQuestionId) FROM examquestions';
	$stmt = $pdo -> prepare($sql);
	$stmt -> execute();
	$result = $stmt -> fetch();
	
	return $result;
	
}


function addExamAnswer(PDO $pdo, $qID, $text, $bool)
{
	$sql = 'INSERT INTO examquestionanswers (ExamQuestionId, Answer, Correct) VALUES (:qID, :text, :bool)';
	$stmt = $pdo->prepare($sql);
	$stmt -> bindParam(':qID', $qID);
	$stmt->bindParam(':text', $text);
	if ($bool == null){
		$bool = 0;
	}else{
		$bool = 1;
	}
	$stmt->bindParam(':bool', $bool);
	$stmt->execute();
}
?>
