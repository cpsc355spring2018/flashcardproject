<?php
$pageTitle = "Create Class";
include 'header.php';

if(isset($_SESSION['key'])){
?>

<style>
#createClass {
 position: relative;
    top: 30%;    
    left:0;
    right:0;
    margin:0 auto;
    width: 25%;
    height: 500px;
  
}

#head{
font-size: 60px;;
text-align: center;

}
#new{
  font-size: 40px;
  color: #d0d0e1;
  padding: 10px;
}
#t,#c,#create{
width: 100%;
}

</style>
<h1 id  = "head"> Create a New Class: </h1>
<form id='createClass' action='createClass.php' method='post' accept-charset='UTF-8'>
	<fieldset>
		<legend id = "new"> New Class </legend>
		<input type = 'hidden' name ='teacherID' id = 'teacherID' maxlength = "50" value = <?php echo $_SESSION['key']?>/>
		<label  for = 'className'> Class Name: </label> <br>
		<input id = "c" type = 'text' name = 'className' id = 'className' maxlength = "50" />
		<br>
		<label  for = 'timeLimit'> Time Limit: </label> <br>
		<input id = "t" type = 'text' name = 'timeLimit' id = 'timeLimit' maxlength = "3" />
		<br>
		<input type = 'hidden' name = 'courseID' id = 'courseID' value = <?php echo $_GET['id'];?>/>
		<br>
		<input id = "create" type = 'submit' name = 'submit' value = "Create"/>
	</fieldset>
</form>
 <?php 
}
else
{
    $_SESSION['error'] = 'You must be logged in to create a class.';
    header("Location: loginHtml.php");
    exit();
}
include 'footer.php';
?>