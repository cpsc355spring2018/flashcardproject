<?php
$pageTitle = "Flashcard Selection";
include 'header.php';
include 'webFunctions.php';

if(isset($_SESSION['key'])){
?>
<style>
#takeQuiz, #viewquiz {
	background: yellow;
	width: 25%;
	height: 40px;
	font-size: 20px;
}

#content {
	top: 30%;
	left: 0;
	right: 0;
	margin: 0 auto;
	width: 35%;
	height: 250px;
}

}
#newContent {
	padding-top: 50px;
}

#e {
    padding-bottom: 20px;
	color: #d0d0e1;
	width: 100%;
}
#drop{
    width: 25%;
}

#abc {
	color: #d0d0e1;
}

#createB {
	color: Yellow;
}

#createB:hover {
	color: #d0d0e1;
}

#Legend {
	font-size: 40px;
	color: #d0d0e1;
	padding: 10px;
}

#s {
	width: 100%;
	background: yellow;
}

#add {
	width: 100%;
}
</style>

<div id="text">
	<h1>Select Flashcards to Study:</h1>
	<?php
$sectionID = $_GET['id'];
$flashcardArray = getFlashcards($pdo, $sectionID);
$totalCards = count($flashcardArray, COUNT_NORMAL);
if(isset($_GET['courseID']))
{
    $courseID = $_GET['courseID'];
}
if(isset($_GET['classID']))
{
    $classID = $_GET['classID'];
    foreach ($flashcardArray as $allFlashcards)
    {
        echo "<a href=flashcardQuestion.php?id=" . $allFlashcards['FlashCardId'] . "&classID=" . $classID . ">" . $allFlashcards['Front'] . "<br><br></a>";
        ?>
		<br>

	<?php
}
if (isset($_SESSION['key']) && is_student_in_class( $pdo, $classID, $_SESSION['key']) && isset($_GET['classID'])) 
	{
	?>
	<form>
		<input id="takeQuiz" type="button" value="Take Quiz" onclick="window.location.href='takeQuiz.php?classID=<?php echo $classID?>&id=<?php echo $sectionID?>&courseID=<?php echo $courseID?>'" />
	</form>
	<?php
	}
}
else
{
    foreach ($flashcardArray as $allFlashcards) 
    {
        echo "<a href=flashcardQuestion.php?id=" . $allFlashcards['FlashCardId'] .">" . $allFlashcards['Front'] . "<br><br></a>";
    }
}

if (isset($_SESSION['admin'])) 
{
    ?>
    
<form>
<br>
		<input id="viewQuiz" type="button" value="View All Quizes"
			onclick="window.location.href='AdminQuiz.php?id=<?php echo $sectionID;?>'" />
	</form>
</div>

<div id="content">
	<form id='flashcards' action='addContent.php?id=flashcard' method='post'
		accept-charset='UTF-8'>
		<fieldset>
			<legend id="legend">Add Flashcard</legend>
			<label id="e">Flashcard Front:</label> 
			<br>
			<input type = 'hidden' name = 'sectionID' value = '<?php echo($sectionID);?>'/>
			<input type='text' name = 'flashcardFront' id='front' maxlength="100" />
			<br>
			<label id = "e"> Flashcard Back: </label>
			<br>
			<input type = 'text' name = 'flashcardBack' id = 'back' maxlength = "100"/>
				<br> <br> 
			<input type='submit' name='Submit' value='Add Flashcard' id="s" />

		</fieldset>
	</form>
</div>

<div id="content">
	<form id='quizzes' action='addContent.php?id=quiz' method='post'
		accept-charset='UTF-8'>
		<fieldset>
			<legend id="legend">Add Quiz</legend>
			<label id= "e"> Questions will be added in the next section. </label>
			<input type ='hidden' name = 'sectionID' value = '<?php echo ($sectionID);?>'>
			<br>
			<label id = "e"> Is the Quiz Active? </label>
			<br>
			<select id="isActive" name="isActive">
			<option disabled selected value>Select:</option>
			<option value="0">No</option>
			<option value="1">Yes</option>
			</select>
			<input type='submit' name='Submit' value='Add Quiz' id="s" />
		</fieldset>
	</form>
</div>
 <?php }
include 'footer.php';
}
else{
    header("Location: index.php");
    exit();
}
?>