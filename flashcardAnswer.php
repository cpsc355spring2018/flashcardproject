<?php
$pageTitle = "Answer";
include 'header.php';
include 'webFunctions.php';

if(isset($_SESSION['key'])){
?>
<style>
#text{
font-size: 35px;
background-image: url('flash.png');
background-size:100% 100%;

}
#aButton{
width: 20%;
height: 30px;
margin-left: 40%;
margin-right: 40%;
}

</style>
<div id = "text">
	<h1>Answer:</h1>
	<?php
	if(isset($_GET['classID']))
	{
	   $flashcardID = $_GET['id'];
	   $classID = $_GET['classID'];
	   $flashcardArray = getFlashcardFront($pdo, $flashcardID);
	   echo $flashcardArray[0]['Back'];
	   ?>
	   </div>
	   <form>
	   		<input id ="aButton" type="button" value="Return to Flashcard Selection" onclick="window.location.href='flashcards.php?classID=<?php echo $classID?>&id=<?php echo $flashcardArray[0]['SectionId']?>'" />
	   </form>
	   <?php 
	}
	else
	{
	    $flashcardID = $_GET['id'];
	    $flashcardArray = getFlashcardFront($pdo, $flashcardID);
	    echo $flashcardArray[0]['Back'];
	    ?>
	   </div>
	   <form>
	   		<input id ="aButton" type="button" value="Return to Flashcard Selection" onclick="window.location.href='flashcards.php?id=<?php echo $flashcardArray[0]['SectionId']?>'" />
	   </form>
	   <?php
	}
include 'footer.php';
}
else{
    header("Location: index.php");
    exit();
}
?>