<?php
session_start();
?>

<html>
<head>
<style type="text/css">


body {
	background: linear-gradient(180deg, #00004d, #666699) fixed;
	background-size: cover;
	height: 100%;
}

a {
	color: #d0d0e1;
	text-decoration: none
}

a:hover {
	color: white;
}

#main {
	width: 100%;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 30px;
	padding-top: 100px;
	padding-bottom: 100px;
	text-align: center;
}

#text {
	width: 100%;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 30px;
	padding-top: 100px;
	padding-bottom: 100px;
	text-align: center;
}
</style>


<script type="text/javascript" async
  src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-MML-AM_CHTML">
</script>
<script>
function myFunction() {
    var str = document.getElementById("demo").innerHTML; 
    var res = str.replace(new RegExp(' ', 'g'), '\\ ');
    document.getElementById("demo").innerHTML = res;
}
</script>

<title><?php echo $pageTitle; ?></title>
</head>


<body>

<?php
if (! isset($pageTitle))
	echo '<h1 style="background:white">$pageTitle must be set before #include "header.php"</h1>';
?>


  <table id = "main"> 
    <tr>
    
      <td> <a href = "index.php"> Home </a> </td>
     
      
      
      <?php 
      if(isset($_SESSION['key'])){?>
          <td> <a href = "StudentsHtml.php"> Students </a> </td>
          <td> <a href = "TeachersHtml.php"> Teachers </a> </td>
          <td> <a href = "logout.php" > Logout </a> </td> 
 <?php }else{?>
  	<td> <a href = "createAccountHtml.php"> Create Account </a> </td>
      <td> <a href = "loginHtml.php"> Login </a> </td> 
 <?php }?>
      
      
    </tr>
  </table>
