<?php
    $pageTitle = "Join With Invite";
    include "webFunctions.php";
	session_start();
	
    $invite = $_POST['code'];
    if(join_class_by_code($pdo, $_SESSION['key'], $invite))
    {
        $_SESSION['join'] = "Successfully joined class!";
        header("Location: StudentsHtml.php");
        exit();
    }
    else
    {
        $_SESSION['error'] = 'Incorrect invite code, please try again';
        header("Location: StudentsHtml.php");
        exit();
    }
?>