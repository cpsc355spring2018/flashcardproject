<?php

function userLogin()
{
    include 'webFunctions.php';
    session_start();

    if ($_POST['email'] == "") 
    {
        $_SESSION['error'] = 'Please enter your email.';
        header("Location: loginHtml.php");
        exit();
    }
    else if ($_POST['password'] == "") 
    {
        $_SESSION['error'] = 'Please enter your password.';
        header("Location: loginHtml.php");
        exit();
    }
    $email = trim($_POST['email']);
    $password = trim($_POST['password']);
        // code to check database for email & password
    if (login_credentials($pdo, $email, $password)) 
    {
        session_regenerate_id (true);
        $sql = 'SELECT Email, UserId, IsAdmin FROM users WHERE Email = :email';
        $s=$pdo ->prepare($sql);
        $s->bindValue(':email', $email);
        $s->execute();
        $out = $s->fetch();
        
        if($out['IsAdmin'] == true)
        {
            $_SESSION['admin'] = true;
        }
        $_SESSION['key'] = $out['UserId'];
        header("Location: courseSelection.php");
    } 
    else 
    {
        $_SESSION['error'] = 'Invalid user name or password';
        header("Location: loginHtml.php");
    }
}
//return true;
userLogin();
?>