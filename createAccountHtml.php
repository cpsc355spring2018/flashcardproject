<?php
$pageTitle = "Create New User";
include 'header.php';
?>

<style>

#signin {
	position: absolute;
	top: 30%;
	left: 0;
	right: 0;
	margin: 0 auto;
	width: 25%;
	padding-top: 70px;
	height: 100%;
}

#legend {
	font-size: 40px;
	color: #d0d0e1;
	padding: 10px;
}

#e {
	color: #d0d0e1;
}

#email, #password, #firstName, #lastName, #confirmPassword {
	width: 100%;
}

#create {
	font-size: 40px;
	color: #d0d0e1;
	padding: 10px;
}

#sButton {
	width: 100%;
}
</style>

<?php
if (isset($_SESSION['error'])) {
    echo '<h2>' . $_SESSION['error'] . '</h2>';
    unset($_SESSION['error']);
}
?>
<div id="signin">
	<form id='createAccount' action='createAccount.php' method='post'
		accept-charset='UTF-8'>
		<fieldset>
			<legend id="create">Create Account</legend>
			<label for='firstName' id="e"> First Name: </label> <input
				type='text' name='firstName' id='firstName' maxlength="50" /> <br>
			<br> <label for='lastName' id="e"> Last Name: </label> <br> <input
				type='text' name='lastName' id='lastName' maxlength="50" /> <input
				type='hidden' name='submitted' id='submitted' value='1' /> <br>
			<br> <label for='email' id="e">Email:</label> <br> <input type='text'
				name='email' id='email' maxlength="100" /><br>
			<br> <label for='password' id="e">Password:</label> <br>
			<input type='password' name='password' id='password' maxlength="50" />
			<br>
			<br> <label for='password' id="e"> Confirm Password:</label><br> <input
				type='password' name='confirmPassword' id='confirmPassword'
				maxlength="50" /> <br>
			<br> <input type='submit' name='Submit' value='Submit' id="sButton" />
		</fieldset>
	</form>
</div>
<?php
include 'footer.php';
?>
